<?php

namespace App;

use App\BaseModel as Model;

class Module extends Model
{
    protected $fillable = [
        'module_alias',
        'module_name',
        'module_core',
        'locked',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->route = 'modules';
        $this->moduleName = trans_choice('modules.title', 1);
    }

    /**
     * One-to-many relation to roles
     *
     * @return \App\Roles intance of Roles;
     */
    public function roles()
    {
        return $this->hasMany('App\Role');
    }
}
