@extends('layouts.index.datatable')

@section('dependencies')
    @php
        $route = 'rolegroups';
        $tableHeader = [
            'name',
            'members',
            'level',
            'action',
        ];

    @endphp
@endsection

@section('top_right_button')
@can('create', new \App\Rolegroup())
    <a href="{{ route($route.'.create') }}" class="btn btn-primary btn-md pull-right">
        <i class="fa fa-fw fa-user-plus"></i> New @choice($route.'.title', 1)
    </a>
@endcan
@endsection

@push('js')
<script>
    datatableSettings = [
        {data: 'rolegroup_name', name: 'Name'},
        {data: 'members', name: 'Used By'},
        {data: 'level', name: 'Level'},
        {data: 'action', name: 'Action', sortable: false, searchable: false},
    ]
</script>
@endpush
