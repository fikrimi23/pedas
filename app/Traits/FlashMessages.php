<?php

namespace App\Traits;

use Flash;

trait FlashMessages
{
    public static function bootFlashMessages()
    {
        // static::observe(\App\Observers\BaseModelObserver::class);
        static::created(function ($model) {
            if ($model->flashDefault ?? false) {
                Flash::push(
                    'success-message',
                    trans('messages.success.create', ['module' => $model->moduleName ?: 'Resource'])
                );
            }
            return true;
        });

        static::updated(function ($model) {
            if ($model->flashDefault ?? false) {
                Flash::push(
                    'success-message',
                    trans('messages.success.update', ['module' => $model->moduleName ?: 'Resource'])
                );
            }
            return true;
        });

        static::deleted(function ($model) {
            if ($model->flashDefault ?? false) {
                Flash::push(
                    'success-message',
                    trans('messages.success.delete', ['module' => $model->moduleName ?: 'Resource'])
                );
            }
            return true;
        });
    }
}
