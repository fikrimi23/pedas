@extends('layouts.index.datatable')

@section('dependencies')
    @php
        $route = 'exams';
        $tableHeader = [
            'course',
            'description',
            'dday',
            'action',
        ];
    @endphp
@endsection

@section('top_right_button')
@can('create', new \App\Exam())
    <a href="{{ route($route.'.create') }}" class="btn btn-primary btn-md pull-right">
        <i class="fa fa-fw fa-user-plus"></i> New @choice($route.'.title', 1)
    </a>
@endcan
@endsection

@push('js')
<script>
datatableSettings = [
    {data: 'course.title', name: 'course'},
    {data: 'description', name: 'Description'},
    {data: 'dday', name: 'Dday'},
    {data: 'action', name: 'Action', sortable: false, searchable: false},
]
</script>
@endpush
