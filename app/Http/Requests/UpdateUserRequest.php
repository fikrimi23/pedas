<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $generalRule = [
            'password' => 'sometimes|min:8|max:36',
            'rolegroup.id' => 'required|exists:rolegroups,id'
        ];

        $specificRule = [];
        switch ($this->method()) {
            case 'POST':
                $specificRule = [
                    'email' => 'required|email|unique:users,email'
                ];
                break;
            case 'PUT':
                $specificRule = [
                    'email' => 'required|email|unique:users,email,'.$this->input('email').',email'
                ];
                break;
        }

        if ($this->has('staff_id')) {
            $specificRule['staff.id'] = 'required|exists:staffs,id';
        } else {
            $specificRule['staff.name'] = 'required';
        }

        return array_merge($generalRule, $specificRule);
    }

    /**
     * Custom messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'A title is required',
            'body.required'  => 'A message is required',
        ];
    }
}
