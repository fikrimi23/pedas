<?php

namespace App\Helper;

class Layouts
{
    /**
     * Make breadcrumbs
     *
     * @param  array  $settings  of Settings
     * @param  boolean $isDefault if default it will use the "name" => "url" method
     * @return string             baked html
     */
    public static function breadcrumbs($settings, $isDefault = false)
    {
        // Define HTML
        $html = '';

        if ($isDefault) {
            // Check for depndencies if not using default breadcrumbs creator
            if (! isset($settings['type'])
                || ! in_array($settings['type'], ['edit', 'create', 'index'])
                || ($settings['type'] == 'edit' ? ! isset($settings['module']) : false)
                || ! isset($settings['route'])
                ) {
                throw new \Exception("Missing parameter", 1);
            }

            // extract route, type, module
            extract($settings);

            // Make first letter uppercase
            $brdSettings = [
                ucfirst($route) => route($route.'.index'),
            ];

            // Switch for edit|create
            if ($type == 'edit') {
                $brdSettings['Edit '.trans_choice($route.'.title', 1)] = route($route.'.edit', $module->id);
            } elseif ($type == 'create') {
                $brdSettings['New '.trans_choice($route.'.title', 1)] = route($route.'.create');
            }
        } else {
            // Use default settings
            $brdSettings = $settings;
        }

        // Iterate through "value" => "url" array
        foreach ($brdSettings as $title => $url) {
            $tmpTitle = $title;
            if ($url !== null) {
                $tmpTitle = '<a href="'.$url.'">'.$title.'</a>';
            }
            $html .= '
                <li class="breadcrumb-item">'.$tmpTitle.'</li>
            ';
        }
        return $html;
    }

    /**
     * Bake css|js
     *
     * @param  string $name plugin_name specified in config/layotus
     * @param  string $type css|js
     * @return string       baked html
     */
    public static function usePlugin($name, $type)
    {
        // get config
        $plugins = config('layouts.plugins');

        if (in_array($type, ['css', 'js']) && in_array($name, array_keys($plugins[$type]))) {
            $bakedPlugin = '';

            // prepare variable
            $plugin = $plugins[$type][$name];

            // prepare template
            $jsTemplate = "<script type='text/javascript' src='%s'></script>";
            $cssTemplate = "<link rel='stylesheet' href='%s'>";

            // bake html by template and config
            if (is_array($plugin)) {
                foreach ($plugin as $url) {
                    $bakedPlugin .= sprintf(${$type."Template"}, asset($url));
                }
            } else {
                $bakedPlugin .= sprintf(${$type."Template"}, asset($plugin));
            }

            return $bakedPlugin;
        }
    }
}
