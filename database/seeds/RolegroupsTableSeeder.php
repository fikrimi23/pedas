<?php

use Illuminate\Database\Seeder;

class RolegroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = DB::table('rolegroups');
        $table->delete();

        $records = [
            [
                'rolegroup_name'   => 'Super Administrator',
                'rolegroup_depth'  => 0,
            ],
            [
                'rolegroup_name'   => 'General Administrator',
                'rolegroup_depth'  => 1,
            ],
        ];

        collect($records)->each(function ($record) {
            App\Rolegroup::create($record);
        });
    }
}
