<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\Uuids;
use App\Traits\FlashMessages;

class User extends Authenticatable
{
    use Notifiable, Uuids, FlashMessages;

    public $incrementing = false;

    public $flashDefault = true;

    public $moduleName = 'User';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'rolegroup_id', 'group_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $attributes = [
        'is_active' => true,
    ];

    /**
     * One-to-one relation to rolegroup
     *
     * @return \App\Rolegroup instance of Rolegroup
     */
    public function rolegroup()
    {
        return $this->belongsTo('App\Rolegroup');
    }

    /**
     * One-to-one relation to staff
     *
     * @return \App\Staff instance of Staff
     */
    public function staff()
    {
        return $this->hasOne('App\Staff');
    }

    /**
     * One-to-one relation to department
     *
     * @return \App\Department instance of Department
     */
    public function department()
    {
        return $this->belongsToMany('App\Department')->withPivot('position');
    }

    public function group()
    {
        return $this->belongsTo('App\Group');
    }

    public function courses()
    {
        return $this->hasMany('\App\Course');
    }
}
