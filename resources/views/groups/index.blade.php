@extends('layouts.dashboard')

@section('title', 'PageTitle')

@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <h4>
                <span class="text-uppercase">Groups</span>
            </h4>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
        @if ($group === null)
            <h3>You didnt join any group</h3>
            <button class="btn btn-dark" data-toggle="modal"  data-target="#createGroup">Create Group</button>
            <button class="btn btn-success" data-toggle="modal"  data-target="#joinGroup">Join Group</button>
        @else
            <h3>{{ $group->name }}</h3>
            <h4>Invite ID: {{ $group->id }}</h4>
            <button class="btn btn-danger" data-toggle="modal"  data-target="#joinGroup">Leave Group</button>

            <h4>Host</h4>
            <h5>{{ $group->host->staff->name }}</h5>
            <h4>Members</h4>
            <ul class="list-group">
                @foreach ($group->members as $member)
                    <li class="list-group-item">{{ $member->staff->name }}</li>
                @endforeach
            </ul>
        @endif
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="joinGroup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        {!! Form::open(['url' => route('groups.store', ['action' => 'join'])]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Join Group</h4>
            </div>
            <div class="modal-body">
                {!! Form::text('group_id', null, ['class' => "form-control", 'placeholder' => "Invite ID"]) !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Join</button>
            </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="createGroup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        {!! Form::open(['url' => route('groups.store')]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Join Group</h4>
            </div>
            <div class="modal-body">
                {!! Form::text('name', null, ['class' => "form-control", 'placeholder' => "Group Name"]) !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
