<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Request;
use App\Role;

class NavbarComposer
{
    private function menu()
    {
        $user = auth()->user();

        $menu = [
            // -- BEGIN -- Dashboard
            [
                'icon' => 'fa-tachometer',
                'title' => trans('navbar.dashboard'),
                'url' => route('dashboard.index'),
                'active' => Request::is('dashboard'),
            ],
            // -- END -- Dashboard
            // -- BEGIN -- Account
            [
                'icon' => 'fa-users',
                'title' => trans('navbar.users.title'),
                'url' => '#',
                'authorized'  => Role::isAuthorized($user, 'modules.index'),

                'active' =>
                    Request::is('staffs*') ||
                    Request::is('users*') ||
                    Request::is('rolegroups*'),
                'subnav' => [
                    [
                        'icon' => 'fa-user',
                        'url' => route('staffs.index'),
                        'authorized'  => Role::isAuthorized($user, 'staffs.index'),
                        'title' => trans('navbar.users.staffs'),
                        'active' => Request::is('staffs*'),
                    ],
                    [
                        'icon' => 'fa-user',
                        'url' => route('users.index'),
                        'authorized'  => Role::isAuthorized($user, 'users.index'),
                        'title' => trans('navbar.users.users'),
                        'active' => Request::is('users*'),
                    ],
                    [
                        'icon' => 'fa-user-plus',
                        'url' => route('rolegroups.index'),
                        'authorized'  => Role::isAuthorized($user, 'rolegroups.index'),
                        'title' => trans('navbar.users.rolegroups'),
                        'active' => Request::is('rolegroups*'),
                    ],
                ],
            ],
            // -- END -- Account
            // -- BEGIN -- Course
            [
                'icon'    => 'fa-list',
                'title'   => "Groups",
                'url'     => route('groups.index'),
                'authorized'  => Role::isAuthorized($user, 'groups.index'),
                'active' => Request::is('groups*'),
            ],
            // -- BEGIN -- Course
            [
                'icon'    => 'fa-list',
                'title'   => "Courses",
                'url'     => route('courses.index'),
                'authorized'  => Role::isAuthorized($user, 'courses.index'),
                'active' => Request::is('courses*'),
            ],
            // -- END -- Course
            // -- BEGIN -- Tasks
            [
                'icon'    => 'fa-edit',
                'title'   => "Tasks",
                'url'     => route('tasks.index'),
                'authorized'  => Role::isAuthorized($user, 'tasks.index'),
                'active' => Request::is('tasks*'),
            ],
            // -- END -- Tasks
            // -- BEGIN -- Exams
            [
                'icon'    => 'fa-newspaper-o',
                'title'   => "Exams",
                'url'     => route('exams.index'),
                'authorized'  => Role::isAuthorized($user, 'courses.index'),
                'active' => Request::is('exams*'),
            ],
            // -- END -- Exams
            // -- BEGIN -- Module
            [
                'icon'    => 'fa-cogs',
                'title'   => trans('navbar.settings.title'),
                'url'     => "#",
                'authorized'  => Role::isAuthorized($user, 'modules.index'),
                'active' => Request::is('modules*'),
                'subnav'  => [
                    [
                        'icon'    => 'fa-puzzle-piece',
                        'url'     => route('modules.index'),
                        'authorized'  => Role::isAuthorized($user, 'modules.index'),
                        'title'   => trans('navbar.settings.modules'),
                        'active' => Request::is('modules*'),
                    ]
                ]
            ],
            // -- END -- Module
        ];

        return $menu;
    }

    public function compose(View $view)
    {
        $html_test = view('layouts.navbar')->with('menu', $this->menu())->render();

        $view->with('navbar', $html_test);
    }
}
