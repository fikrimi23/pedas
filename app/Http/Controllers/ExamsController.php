<?php

namespace App\Http\Controllers;

use App\Exam;
use Illuminate\Http\Request;
use Datatables;
use DB;
use App\Helper\Flash;

class ExamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('exams.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = \App\Course::where('user_id', auth()->id())
            ->get()
            ->pluck('title', 'id');
        if (count($courses) == 0) {
            Flash::push('error-message', 'Create course first');
            return back();
        }
        return view('exams.form')->with('courses', $courses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $exam = DB::transaction(function() use ($request) {
            $exam = Exam::create([
                'course_id' => $request->input('course_id'),
                'description' => $request->input('description'),
                'dday' => $request->input('dday'),
            ]);

            return $exam;
        });

        return redirect()->route('exams.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function show(Exam $exam)
    {
        return view('exams.show')->with('exam', $exam);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function edit(Exam $exam)
    {
        $courses = \App\Course::where('user_id', auth()->id())
            ->get()
            ->pluck('title', 'id');

        return view('exams.form')->with([
            'exam'      => $exam,
            'courses'    => $courses
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exam $exam)
    {
        $exam = DB::transaction(function () use ($exam, $request) {
            $exam->update($request->except('isEdit'));
            return $exam;
        });

        return redirect()->route('exams.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exam $exam)
    {
        DB::transaction(function () use ($exam) {
            $exam->delete();
        });

        return redirect()->back();
    }

    public function getDatatablesResources()
    {
        $collection = Exam::with('course')->whereHas('course', function ($course) {
            return ($course->where('user_id', auth()->id()));
        })->get();

        return Datatables::of($collection)
            ->addColumn('action', function ($module) {
                $settings = [
                    'title' => 'exams',
                    'module' => $module,
                    'part' => [
                        'view' => true,
                        'edit' => true,
                        'delete' => true,
                    ]
                ];
                $html = view('layouts.datatablesActionColumn')->with($settings)->render();

                return $html;
            })
            ->removeColumn('id')
            ->make(true);
    }
}
