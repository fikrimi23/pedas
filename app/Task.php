<?php

namespace App;

use App\BaseModel as Model;

class Task extends Model
{
    protected $guarded = [];

    public $timestamps = true;

    public $route = 'tasks';

    protected $dates = [
        'deadline',
        'created_at',
        'updated_at',
    ];

    public function course()
    {
        return $this->belongsTo('App\Course');
    }

    public function getCompleteStatusAttribute()
    {
        $now = \Carbon\Carbon::now();
        $then = $this->deadline;
        $diff = $then->diff($now);

        if ($now < $then) {
            $time = " to go";
        } else {
            $time = " past";
        }
        $format = "<span class='label label-%s'>%s</span>";

        if ($this->status == '1') {
            $status = sprintf($format, 'success', 'Completed');
        } else {
            if ($now > $then) {
                $status = sprintf($format, 'danger', 'Uncompleted');
            } else {
                $status = sprintf($format, 'warning', 'Uncompleted');
            }
        }


        return $diff->format('%a days').$time." ".$status;
    }
}
