<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $generalRule = [
            'title' => 'required',
            'institute.id' => 'required|exists:institutes,id'
        ];

        $specificRule = [];
        // switch ($this->method()) {
        //     case 'POST':
        //         break;
        //     case 'PUT':
        //         break;
        // }

        return array_merge($generalRule, $specificRule);
    }
}
