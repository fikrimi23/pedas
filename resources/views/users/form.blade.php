@extends('layouts.dashboard')

@section('css')
{!! Layouts::usePlugin('select2', 'css') !!}
@endsection

@section('breadcrumbs')
{!! Layouts::breadcrumbs(['type' => $isEdit ? 'edit' : 'create', 'module' => $isEdit ? $user : null, 'route' => 'users'], true) !!}
@endsection

@section('content')
<section class="content">
    <div class="row m-b-sm">
        <div class="col-xs-6">
            <h4>
                {{ $isEdit ? "EDIT" : "CREATE NEW" }} <span class="text-uppercase">@choice('users.title', 1)</span>
            </h4>
        </div>
        @if ($isEdit)
        <div class="col-xs-6">
            {!! Form::open(['url' => route('users.destroy', $user->id), 'method'=>'delete' ,]) !!}
                <a
                    href="#"
                    data-confirm="Are you sure to delete this resources?"
                    class="btn btn-danger btn-md pull-right js-delete-confirm"
                    ><i class="icon-trash"></i> Delete
                    </a>
            {!! Form::close() !!}
        </div>
        @endif
    </div>
    <div class="row">
        <div class="col-xs-12">
            @if ($isEdit)
                {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'put', 'class' => 'parsley-validate']) !!}
                {!! Form::hidden('isEdit', true) !!}
            @else
                {!! Form::open(['route' => ['users.store'], 'method' => 'post', 'class' => 'parsley-validate']) !!}
            @endif
            <div class=x_panel>
                <div class=x_title>
                    <strong>@choice('users.title', 1) Properties</strong>
                    <small>Form</small>
                </div>
                <div class=x_content>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                {!! Form::label('email', 'Email', ['class' => 'form-control-label']) !!}
                                {!! Form::email('email', null,
                                    [
                                    'class' => 'form-control',
                                    'data-parsley-required',
                                    'data-parsley-email',
                                    'autocomplete' => 'off'
                                    ]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('password', "Password", ['class' => 'form-control-label']) !!}
                                {!! Form::password('password',
                                    [
                                    'class' => 'form-control',
                                    $isEdit ? '' : 'data-parsley-required',
                                    'minlength' => '8',
                                    'maxlength' => '36',
                                    'autocomplete' => 'off'
                                    ]) !!}
                            </div>
                            @if ($isEdit)
                                @if ($user->rolegroup->rolegroup_depth > 0 )
                                <div class="form-group">
                                    {!! Form::label('rolegroup[id]', "Rolegroup", ['class' => 'form-control-label']) !!}
                                    {!! Form::select('rolegroup[id]', $rolegroups, null,
                                        [
                                        'class' => 'form-control select2',
                                        'data-parsley-required',
                                        'minlength' => '8',
                                        'maxlength' => '36',
                                        'autocomplete' => 'off'
                                        ]) !!}
                                </div>
                                @else
                                    {!! Form::hidden('rolegroup[id]', $user->rolegroup->id) !!}
                                @endif
                            @else
                                <div class="form-group">
                                    {!! Form::label('rolegroup[id]', "Rolegroup", ['class' => 'form-control-label']) !!}
                                    {!! Form::select('rolegroup[id]', $rolegroups, null,
                                        [
                                        'class' => 'form-control select2',
                                        'data-parsley-required',
                                        'minlength' => '8',
                                        'maxlength' => '36',
                                        'autocomplete' => 'off'
                                        ]) !!}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class=x_panel>
                <div class=x_title>
                    <strong>@choice('staffs.title', 1) Properties</strong>
                    <small>Form</small>
                </div>
                <div class=x_content>
                    @if (! $isEdit)
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    {!! Form::label('staff[id]', 'Profile', ['class' => 'form-control-label']) !!}
                                    {!! Form::select('staff[id]', $staffs, null,
                                        [
                                        'class' => 'form-control select2',
                                        ]) !!}
                                </div>
                            </div>
                        </div>
                    <div class="line line-dashed line-lg pull-in"></div>
                    @endif
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                {!! Form::label('staff[name]', 'Name', ['class' => 'form-control-label']) !!}
                                {!! Form::text('staff[name]', null,
                                    [
                                    'class' => 'form-control',
                                    'data-parsley-required',

                                    ]) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-md btn-primary"><i class="fa fa-dot-circle-o"></i> Submit</button>
            <button type="reset" class="btn btn-md btn-danger"><i class="fa fa-ban"></i> Reset</button>
            @if ($isEdit)
                <a href="{{ route('users.index') }}" type="button" class="btn btn-md btn-secondary"><i class="fa fa-arrow-left"></i> Back</a>
            @endif
            {!! Form::close() !!}
        </div>
    </div>
</section>
@endsection

@section('js')
{!! Layouts::usePlugin('select2', 'js') !!}
{!! Layouts::usePlugin('parsley', 'js') !!}
@endsection
