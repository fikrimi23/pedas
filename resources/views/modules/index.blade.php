@extends('layouts.index.datatable')

@section('dependencies')
    @php
        $route = 'modules';
        $tableHeader = [
            'name',
            'alias',
            'locked',
            'action',
        ];
    @endphp
@endsection

@section('top_right_button')
@can('create', new \App\Module())
    <a href="{{ route($route.'.create') }}" class="btn btn-primary btn-md pull-right">
        <i class="fa fa-fw fa-user-plus"></i> New @choice($route.'.title', 1)
    </a>
@endcan
@endsection

@push('js')
<script>
datatableSettings = [
    {data: 'module_name', name: 'Name'},
    {data: 'module_alias', name: 'Alias'},
    {data: 'locked', name: 'Locked'},
    {data: 'action', name: 'Action', sortable: false, searchable: false},
]
</script>
@endpush
