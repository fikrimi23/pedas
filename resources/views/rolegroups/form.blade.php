@extends('layouts.dashboard')

@section('css')
{!! Layouts::usePlugin('select2', 'css') !!}
@endsection

@section('breadcrumbs')
{!! Layouts::breadcrumbs(['type' => $isEdit ? 'edit' : 'create', 'module' => $isEdit ? $rolegroup : null, 'route' => 'rolegroups'], true) !!}
@endsection


@section('content')
<section class="content">
    <div class="row m-b-sm">
        <div class="col-xs-6">
            <h4>
                {{ $isEdit ? "EDIT" : "CREATE NEW" }} <span class="text-uppercase">@choice('rolegroups.title', 1)</span>
            </h4>
        </div>
        @if ($isEdit)
        @can('delete', $rolegroup)
            <div class="col-xs-6">
                {!! Form::open(['url' => route('rolegroups.destroy', $rolegroup->id), 'method'=>'delete' ,]) !!}
                    <a
                        href="#"
                        data-confirm="Are you sure to delete this resources?"
                        class="btn btn-danger btn-md pull-right js-delete-confirm"
                        ><i class="icon-trash"></i> Delete
                        </a>
                {!! Form::close() !!}
            </div>
        @endcan
        @endif
    </div>
    <div class="row">
        <div class="col-xs-12">
            @if ($isEdit)
                {!! Form::model($rolegroup, ['route' => ['rolegroups.update', $rolegroup->id], 'method' => 'put', 'class' => 'parsley-validate']) !!}
                {!! Form::hidden('isEdit', true) !!}
            @else
                {!! Form::open(['route' => ['rolegroups.store'], 'method' => 'post', 'class' => 'parsley-validate']) !!}
            @endif
            <div class=x_panel>
                <div class=x_title>
                    <strong>@choice('rolegroups.title', 1) Properties</strong>
                    <small>Form</small>
                </div>
                <div class=x_content>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                {!! Form::label('rolegroup_name', trans_choice('rolegroups.title', 1).' name', ['class' => 'form-control-label']) !!}
                                {!! Form::text('rolegroup_name', null,
                                    [
                                    'class' => 'form-control',
                                    'data-parsley-required'
                                    ]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('', trans_choice('rolegroups.title', 1)." level", ['class' => 'form-control-label']) !!}
                                {!! Form::number('rolegroup_depth', null,
                                    [
                                    'class' => 'form-control',
                                    'min' => '0',
                                    'max' => '99',
                                    'data-parsley-required',
                                    ]) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if ($isEdit)
                <div class=x_panel>
                    <div class=x_title>
                        <strong>@choice('rolegroups.title', 1) Abilities</strong>
                        <small>Form</small>
                    </div>
                    <div class=x_content>
                        <div class="row">
                            <div class="col-xs-10">
                                {!! Form::select('addAbilities', $modules->except($rolegroup->roles->pluck('module_id')->toArray()), '', ['id' => 'addAbilities', 'class' => 'form-control select2', 'multiple' => 'multiple']) !!}
                            </div>
                            <div class="col-xs-2">
                                {!! Form::button('<i class="icon-plus"></i> Add Module', [
                                    'id' => 'add-module-btn',
                                    'class' => 'btn btn-primary btn-block',
                                    'data-url' => route('rolegroups.addModules', ['id' => $rolegroup->id]),
                                    'data-csrf' => csrf_token() ]) !!}
                            </div>
                        </div>
                        <div class="line line-dashed line-lg pull-in"></div>
                        @foreach ($rolesCooked as $module_id => $roles)
                        <div class="row m-b" id="module_{{ $module_id }}">
                            <div class="col-xs-6">
                                {!! Form::select($module_id, [$roles[0]->module->id => $roles[0]->module->module_name], $roles[0]->module->id, ['class' => 'form-control', 'disabled']) !!}
                            </div>
                            <div class="col-xs-5">
                                {!! Form::select("abilities[".$module_id."][]", $abilities, collect($roles)->pluck('role_ability')->toArray(), ['class' => 'form-control select2', 'multiple']) !!}
                            </div>
                            <div class="col-xs-1">
                                {!! Form::button('<i class="icon-trash"></i>', [
                                    'id' => 'add-module-btn',
                                    'class' => 'btn btn-block btn-danger btn-delete',
                                    'data-url' => route('rolegroups.removeModules', ['id' => $rolegroup->id]),
                                    'data-csrf' => csrf_token(),
                                    "data-module-name" => $roles[0]->module->module_name,
                                    "data-module-id" => $roles[0]->module->id ]) !!}
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            @endif

            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Submit</button>
            <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Reset</button>

            {!! Form::close() !!}
        </div>
    </div>
</section>
@endsection

@push('js')
{!! Layouts::usePlugin('select2', 'js') !!}
{!! Layouts::usePlugin('parsley', 'js') !!}

<script>
    $(document).ready(function() {
        $('#add-module-btn').click(function(event) {
            event.preventDefault();

            var
                url = $(this).data('url');
                data = {
                    module_id: $('select').val(),
                    _token: $(this).data('csrf')
                }
            $.post(url, data, function(data) {
                if (data.status) {
                    location.reload();
                }
            });
        });

        $(".btn-delete").click(function(event) {
            event.preventDefault();
            var
                url = $(this).data('url')
                moduleName = $(this).data('module-name')
                moduleId = $(this).data('module-id')
                data = {
                    module_id: moduleId,
                    _token: $(this).data('csrf')
                }
            $.post(url, data, function(data, textStatus, xhr) {
                if (data.status) {
                    $('#module_'+moduleId).remove();
                    $('#addAbilities')
                        .append($("<option></option>")
                        .attr("value", moduleId)
                        .text(moduleName))
                        .select2();
                }
            });
        });

    });
</script>
@endpush
