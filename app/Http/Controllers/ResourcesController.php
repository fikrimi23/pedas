<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

use DB;
use Datatables;

class ResourcesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('resources.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = \App\Course::where('user_id', auth()->id())
            ->get()
            ->pluck('title', 'id');

        if (count($courses) == 0) {
            \Flash::push('error-message', 'Create course first');
            return back();
        }

        return view('resources.form')
            ->with('courses', $courses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = DB::transaction(function () use ($request) {
            // ddr($request);
            $task = Task::create([
                'course_id' => $request->input('course_id'),
                'description' => $request->input('description'),
                'deadline' => $request->input('deadline'),
                'status' => '1'
                ]);
            return $task;
        });

        return redirect()->route('resources.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        $courses = \App\Course::where('user_id', auth()->id())->get()->pluck('title', 'id');
        return view('resources.form')->with([
            'task' => $task,
            'courses' => $courses,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $task = DB::transaction(function () use ($task, $request) {
            $task->update($request->except('isEdit'));
            return $task;
        });
        return redirect()->route('resources.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        DB::transaction(function () use ($task) {
            $task->delete();
        });
        return redirect()->back();
    }

    /**
     * Get resources for datatables
     *
     * @return JSON Datatables JSON data
     */
    public function getDatatablesResources()
    {
        $collection = Task::with('course')->whereHas('course', function ($course) {
            return ($course->where('user_id', auth()->id()));
        })->get();

        return Datatables::of($collection)
            ->editColumn('status', function ($module) {
                $now = Carbon::now();
                $then = $module->deadline;
                $diff = $then->diff($now);

                if ($now < $then) {
                    $status = " to go";
                } else {
                    $status = " past";
                }

                return $diff->format('%a days').$status;
            })
            ->addColumn('action', function ($module) {
                $settings = [
                    'title' => 'tasks',
                    'module' => $module,
                    'part' => [
                        'edit' => true,
                        'delete' => true,
                    ]
                ];
                $html = view('layouts.datatablesActionColumn')->with($settings)->render();

                return $html;
            })
            ->removeColumn('id')
            ->make(true);
    }
}
