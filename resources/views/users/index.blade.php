@extends('layouts.index.datatable')

@section('dependencies')
    @php
        $route = 'users';
        $tableHeader = [
            'email',
            'active',
            'action',
        ];
    @endphp
@endsection

@section('top_right_button')
<a href="{{ route('users.create') }}" class="btn btn-primary btn-sm pull-right">
    <i class="fa fa-fw fa-user"></i> New @choice('users.title', 1)
</a>
@endsection

@push('js')
<script>
    datatableSettings = [
        {data: 'email'},
        {data: 'is_active'},
        {data: 'action', sortable: false, searchable: false},
    ]
</script>
@endpush
