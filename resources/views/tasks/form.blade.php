@extends('layouts.dashboard')

@section('css')
@endsection

@section('breadcrumbs')
{!! Layouts::breadcrumbs(['type' => $isEdit ? 'edit' : 'create', 'module' => $isEdit ? $task : null, 'route' => 'tasks'], true) !!}
@endsection

@section('content')
<section class="content">
    <h4>{{ $isEdit ? "EDIT" : "CREATE NEW" }} TASKS</h4>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            @if ($isEdit)
                {!! Form::model($task, ['route' => ['tasks.update', $task->id], 'method' => 'put']) !!}
                {!! Form::hidden('isEdit', true) !!}
            @else
                {!! Form::open(['route' => ['tasks.store'], 'method' => 'post']) !!}
            @endif
            {!! Form::token() !!}
            <div class=x_panel>
                <div class=x_title>
                    <strong>Course Properties</strong>
                    <small>Form</small>
                </div>
                <div class=x_content>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group form-float">
                                {!! Form::label('', 'Course name', ['class' => 'form-label']) !!}
                                {!! Form::select('course_id', $courses, null, ['class' => 'form-control select2']) !!}
                            </div>
                            <div class="form-group form-float">
                                {!! Form::label('', "Description", ['class' => 'form-label']) !!}
                                {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Task Description']) !!}
                            </div>
                            <div class="form-group form-float">
                                {!! Form::label('', "Deadline", ['class' => 'form-label']) !!}
                                {!! Form::date('deadline', $isEdit ? $task->deadline : null, ['class' => 'form-control', 'placeholder' => 'Task Deadline']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Submit</button>
                    <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Reset</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</section>
@endsection

@push('js')
@endpush
