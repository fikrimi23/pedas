@extends('layouts.index.datatable')

@section('dependencies')
    @php
        $route = 'tasks';
        $tableHeader = [
            'course',
            'deadline',
            'status',
            'action',
        ];
    @endphp
@endsection

@section('top_right_button')
@can('create', new \App\Task())
    <a href="{{ route($route.'.create') }}" class="btn btn-primary btn-md pull-right">
        <i class="fa fa-fw fa-user-plus"></i> New @choice($route.'.title', 1)
    </a>
@endcan
@endsection



@push('js')
<script>
datatableSettings = [
    {data: 'course.title', name: 'course'},
    {data: 'deadline', name: 'deadline'},
    {data: 'status', name: 'status'},
    {data: 'action', name: 'Action', sortable: false, searchable: false},
]
</script>
@endpush
