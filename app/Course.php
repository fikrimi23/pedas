<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use App\BaseModel as Model;

class Course extends Model
{
    protected $guarded = [];

    public $route = 'courses';

    public function user()
    {
        return $this->hasOne('\App\User');
    }

    public function tasks()
    {
        return $this->hasMany('\App\Task');
    }

    public function exams()
    {
        return $this->hasMany('\App\Exam');
    }

    public function getTimeRangeAttribute()
    {
        $start = new \Carbon\Carbon($this->time);
        $end = (new \Carbon\Carbon($this->time))->modify("+ ".$this->duration." min");
        return $start->format('H:i')." - ".$end->format('H:i');
    }
}
