<?php

return [
    'title'          => 'Rolegroup|Rolegroups',
    'messages' => [
        'delete_locked'    => 'You cannot delete locked Rolegroup',
    ]
];
