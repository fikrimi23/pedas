@extends('layouts.dashboard')

@section('css')
@endsection

@section('breadcrumbs')
{!! Layouts::breadcrumbs(['type' => $isEdit ? 'edit' : 'create', 'module' => $isEdit ? $course : null, 'route' => 'courses'], true) !!}
@endsection

@section('content')
<section class="content">
    <h4>{{ $isEdit ? "EDIT" : "CREATE NEW" }} COURSES</h4>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            @if ($isEdit)
                {!! Form::model($course, ['route' => ['courses.update', $course->id], 'method' => 'put']) !!}
                {!! Form::hidden('isEdit', true) !!}
            @else
                {!! Form::open(['route' => ['courses.store'], 'method' => 'post']) !!}
            @endif
            {!! Form::token() !!}
            <div class=x_panel>
                <div class=x_title>
                    <strong>Course Properties</strong>
                    <small>Form</small>
                </div>
                <div class=x_content>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group form-float">
                                {!! Form::label('', 'Course name', ['class' => 'form-label']) !!}
                                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Course Name']) !!}
                            </div>
                            <div class="form-group form-float">
                                {!! Form::label('', "Lecturer", ['class' => 'form-label']) !!}
                                {!! Form::text('lecturer', null, ['class' => 'form-control', 'placeholder' => 'Course Lectuer']) !!}
                            </div>
                            <div class="form-group form-float">
                                {!! Form::label('', "Room", ['class' => 'form-label']) !!}
                                {!! Form::text('room', null, ['class' => 'form-control', 'placeholder' => 'Course Room']) !!}
                            </div>
                            <div class="form-group form-float">
                                {!! Form::label('', "Day", ['class' => 'form-label']) !!}
                                {!! Form::select('day', [
                                    '1' => "Senin",
                                    '2' => "Selasa",
                                    '3' => "Rabu",
                                    '4' => "Kamis",
                                    '5' => "Jumat",
                                    '6' => "Sabtu",
                                    '7' => "Minggu",
                                ], null, ['class' => 'form-control', 'placeholder' => 'Course Day']) !!}
                            </div>
                            <div class="form-group form-float">
                                {!! Form::label('', "Time", ['class' => 'form-label']) !!}
                                {!! Form::time('time', null, ['class' => 'form-control', 'placeholder' => 'Course Time']) !!}
                            </div>
                            <div class="form-group form-float">
                                {!! Form::label('', "Duration", ['class' => 'form-label']) !!}
                                {!! Form::number('duration', null, ['class' => 'form-control', 'placeholder' => 'Course Duration']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Submit</button>
                    <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Reset</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</section>
@endsection

@section('js')
@endsection
