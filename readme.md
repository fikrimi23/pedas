# Pedas 

Personal Digital Assistant for School

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

- Composer
- PHP 7
- PostgreSQL (preferred)

### Installing

A step by step series of examples that tell you have to get a development env running

1. Clone this repo
2. Run ```composer install```
3. Set up database in config/database.php
4. Run ```php artisan migrate```

You should now deployed SITH to your system

## Deployment

Nothing special

## Built With

* [Laravel 5.4](https://laravel.com/docs/5.4) - Backend Framework
* [Composer](https://getcomposer.org/doc/) - Dependency Management
* [Bootstrap 4](http://v4-alpha.getbootstrap.com) - Front End Framework
* [Core UI](http://coreui.io) - Template
