<?php

namespace App;

use App\BaseModel as Model;

// use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $guarded = [];

    public $timestamps = true;

    public $route = 'exams';


    protected $dates = [
        'dday', 'created_at', 'updated_at'
    ];

    public function course()
    {
        return $this->belongsTo('App\Course');
    }
}
