@extends('layouts.dashboard')

@section('title', 'PageTitle')

@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <h4>
                <span class="text-uppercase">@lang('dashboard.title')</span>
            </h4>
        </div>
    </div>

    <h1>{{ $welcome }}, Fikri.</h1>
    <hr>
    <h4>Here's a list of what you have to do. Have a nice day!</h4>
    <div class="row">
        <div class="col-sm-4">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-pencil-square-o"></i>
                </div>
                <div class="count">{{ $tasks_count }}</div>

                <h3>Uncompleted Tasks</h3>
                <p>from total tasks.</p>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-newspaper-o"></i>
                </div>
                <div class="count">{{ $exams_count }}</div>
                <h3>Upcoming Exams</h3>
                <p>from total exams.</p>
            </div>
        </div>
    </div>
    <div class="row" style="margin-left: 0px;">
        <hr>
    </div>
    <div class="row" style="margin-top: 14px;">
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>Today's Courses</h2>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <th>Mata Kuliah</th>
                            <th>Jam</th>
                            <th>Ruangan</th>
                        </thead>
                        <tbody>
                            @foreach ($today_courses as $course)
                            <tr>
                                <td>{{ $course->title }}</td>
                                <td>{{ $course->time_range }}</td>
                                <td>{{ $course->room }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>Tasks in 7 days</h2>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <th>Mata Kuliah</th>
                            <th>Status</th>
                        </thead>
                        <tbody>
                            @foreach ($tasks as $task)
                                <tr>
                                    <td>{{ $task->course->title }}</td>
                                    <td>{!! $task->complete_status !!}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>Exams in 7 days</h2>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <th>Mata Kuliah</th>
                            <th>Tanggal</th>
                        </thead>
                        <tbody>
                            @foreach ($exams as $exam)
                                <tr>
                                    <td>{{ $exam->course->title }}</td>
                                    <td>{{ $exam->dday->format('d-m-Y') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <hr>
    </div>
</section>
@endsection
