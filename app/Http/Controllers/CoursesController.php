<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\UpdateCourseRequest;
use App\Course;
use Datatables;
use DB;
use App\Helper\Flash;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('courses.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('courses.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdateCourseRequest $request)
    {
        $course = DB::transaction(function () use ($request) {
            $course = Course::create(array_merge($request->all(), ['user_id' => auth()->id()]));

            $group = auth()->user()->group;
            $group->load(['members']);

            if ($group !== null) {
                $course->update(['group_id' => \Webpatser\Uuid\Uuid::generate(4)->string]);

                foreach ($group->members as $member) {
                    if (! $member->courses->contains('group_id', $course->group_id)) {
                        $course = new Course($course->toArray());
                        $course->user_id = $member->id;
                        $course->save();
                    }
                }
            }

            return $course;
        });

        return redirect()->route('courses.index')->with('course', $course);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        return view('courses.show')->with('course', $course);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        return view('courses.form')->with('course', $course);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCourseRequest $request, Course $course)
    {
        $course = DB::transaction(function () use ($course, $request) {
            $course->update($request->except('isEdit'));

            $group = auth()->user()->group;
            $group->load(['members']);

            if ($group !== null) {
                foreach ($group->members as $member) {
                    $member->load(['courses']);
                    if ($member->courses->contains('group_id', $course->group_id)) {
                        $memberCourse = Course::where('group_id', $course->group_id)->first();
                        $memberCourse->update(collect($course->toArray())->except('user_id')->toArray());
                    }
                }
            }

            return $course;
        });
        return redirect()->route('courses.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        DB::transaction(function() use ($course) {
            $course->delete();

            $group = auth()->user()->group;
            $group->load(['members']);

            if ($group !== null) {
                foreach ($group->members as $member) {
                    $member->load(['courses']);
                    if ($member->courses->contains('group_id', $course->group_id)) {
                        $memberCourse = Course::where('group_id', $course->group_id)->first();
                        $memberCourse->delete();
                    }
                }
            }

        });
        return redirect()->back();
    }

    /**
     * Get resources for datatables
     *
     * @return JSON Datatables JSON data
     */
    public function getDatatablesResources()
    {
        return Datatables::of(Course::where('user_id', auth()->id())->get([
            'id', 'title', 'lecturer', 'room', 'day', 'time'
            ]))
            ->addColumn('action', function ($module) {
                $settings = [
                    'title' => 'courses',
                    'module' => $module,
                    'part' => [
                        'edit' => true,
                        'delete' => true,
                    ]
                ];
                $html = view('layouts.datatablesActionColumn')->with($settings)->render();

                return $html;
            })
            ->removeColumn('id')
            ->make(true);
    }
}
