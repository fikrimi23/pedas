<?php

namespace App\Policies;

use App\User;
use App\Role;
use App\BaseModel;
use Illuminate\Auth\Access\HandlesAuthorization;

class BaseModelPolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can view the department.
     *
     * @param  \App\User  $user
     * @param  \App\BaseModel  $model
     * @return mixed
     */
    public function view(User $user, BaseModel $model)
    {
        return Role::isAuthorized($user, $model->route.'.show');
    }

    /**
     * Determine whether the user can create departments.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user, BaseModel $model)
    {
        return Role::isAuthorized($user, $model->route.'.create');
    }

    /**
     * Determine whether the user can update the department.
     *
     * @param  \App\User  $user
     * @param  \App\BaseModel  $model
     * @return mixed
     */
    public function update(User $user, BaseModel $model)
    {
        return Role::isAuthorized($user, $model->route.'.update');
    }

    /**
     * Determine whether the user can delete the department.
     *
     * @param  \App\User  $user
     * @param  \App\BaseModel  $model
     * @return mixed
     */
    public function delete(User $user, BaseModel $model)
    {
        return Role::isAuthorized($user, $model->route.'.destroy');
    }

    public function before($user, $ability, BaseModel $model)
    {
        if (! isset($model->route)) {
            throw new \Exception("Model Resource Route is not Defined", 1);
        }

        return null;
    }
}
