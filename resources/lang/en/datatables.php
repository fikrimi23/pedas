<?php

return [
    "lengthMenu"     => "Display _MENU_ entries",
    "zeroRecords"    => "Nothing found - sorry",
    "info"           => "Showing page _PAGE_ of _PAGES_",
    "infoEmpty"      => "No records available",
    "infoFiltered"   => "(filtered from _MAX_ total records)",
    "emptyTable"     => "No data available in table",
    "loadingRecords" => "Loading...",
    "processing"     => "Processing...",
    "search"         => "Search:",

    "paginate" => [
        "first"    => "First",
        "last"     => "Last",
        "next"     => "Next",
        "previous" => "Previous"
    ],

    'header' => [
        'action' => 'Action',
        'active' => 'Active',
        'alias'  => 'Alias',
        'email'  => 'Email',
        'level'  => 'Level',
        'locked' => 'Locked',
        'name'   => 'Name',
        'title'  => 'Title',
        'members' => 'Members',
        'institute' => 'Insitute',
        'course' => "Courses",
        'lecturer' => "Lecturer",
        'room' => "Room",
        'day' => "Day",
        'time' => "Time",
        'description' => "Description",
        'dday' => "Due Day",
    ],
];
