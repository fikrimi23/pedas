// window.ParsleyConfig = {

// };


$(document).ready(function() {
    // Initiator for select 2
    if (jQuery().select2) {
        $.fn.select2.defaults.set( "theme", "bootstrap");
        $('.select2').select2({
            'theme': 'bootstrap',
        })
    }

    // Initiator for parsley
    if (jQuery().parsley) {
        $('.parsley-validate').parsley()
    }

    // Other function
    $('.select2').on('select2:unselect', function(event) {
        $el = $(this)
        $('.select2-search__field').val('');
        $(this).select2();
        $(this).focus();
    });

    $('.select2').on('select2:select', function(event) {
        $(this).focus();
    });
});

// Confirmation before delete for current and future element
$(document).on('click', '.js-delete-confirm', function(event) {
    event.preventDefault();
    var
        self = $(this);
        $form = self.closest('form');
        tempElement = $("<input type='hidden'/>");
        messages = self.attr('data-confirm') ? self.data('confirm') : "Are you sure to delete this resources?";

    console.log(messages);

    // clone the important parts of the button used to submit the form.
    tempElement
        .attr("name", self.data('name'))
        .attr("value", self.data('value'))
        .appendTo($form);
    bootbox.confirm({
        message: "<p class='m-t-sm'>"+messages+"</p>",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'bg-teal'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function(choice) {
            if (choice) {
                $form.submit();
            }
        }
    });
});
