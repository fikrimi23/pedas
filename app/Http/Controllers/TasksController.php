<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

use DB;
use Datatables;
use Carbon\Carbon;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax() || $request->has('action')) {
            if ($request->input('action') == 'complete') {
                Task::findOrFail($request->input('task_id'))->update([
                    'status' => '1',
                    ]);
            }
        }
        return view('tasks.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = \App\Course::where('user_id', auth()->id())
            ->get()
            ->pluck('title', 'id');

        if (count($courses) == 0) {
            \Flash::push('error-message', 'Create course first');
            return back();
        }

        return view('tasks.form')
            ->with('courses', $courses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = DB::transaction(function () use ($request) {
            // ddr($request);
            $task = Task::create([
                'course_id' => $request->input('course_id'),
                'description' => $request->input('description'),
                'deadline' => $request->input('deadline'),
                'status' => '0'
                ]);
            return $task;
        });

        return redirect()->route('tasks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        $courses = \App\Course::where('user_id', auth()->id())->get()->pluck('title', 'id');
        return view('tasks.form')->with([
            'task' => $task,
            'courses' => $courses,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $task = DB::transaction(function () use ($task, $request) {
            $task->update($request->except('isEdit'));
            return $task;
        });
        return redirect()->route('tasks.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        DB::transaction(function () use ($task) {
            $task->delete();
        });
        return redirect()->back();
    }

    /**
     * Get resources for datatables
     *
     * @return JSON Datatables JSON data
     */
    public function getDatatablesResources()
    {
        $now = \Carbon\Carbon::now();
        $collection = Task::with('course')
            ->where(function ($task) use ($now) {
                return $task->where('deadline', ">", $now)->orWhere('status', '0');
            })
            ->whereHas('course', function ($course) {
                return ($course->where('user_id', auth()->id()));
            })
            ->get();

        return Datatables::of($collection)
            ->editColumn('status', function ($module) {
                return $module->complete_status;
            })
            ->addColumn('action', function ($module) {
                $markUrl = route('tasks.index', ['action' => 'complete', 'task_id' => $module->id]);
                $settings = [
                    'title' => 'tasks',
                    'module' => $module,
                    'part' => [
                        'edit' => true,
                        'delete' => true,
                        'custom' => $module->status ? '' :
                        "
                        <a
                            class='m-l-5 btn-link'
                            href='{$markUrl}'
                            data-toggle='tooltip'
                            data-placement='bottom'
                            title='View'>
                            <i class='m-r-sm fa fa-lg fa-check-circle-o'></i>Mark as Completed
                            </a>
                        ",
                    ]
                ];
                $html = view('layouts.datatablesActionColumn')->with($settings)->render();

                return $html;
            })
            ->removeColumn('id')
            ->make(true);
    }
}
