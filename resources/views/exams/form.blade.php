@extends('layouts.dashboard')

@section('css')
{!! Layouts::usePlugin('select2', 'css') !!}
@endsection

@section('breadcrumbs')
{!! Layouts::breadcrumbs(['type' => $isEdit ? 'edit' : 'create', 'module' => $isEdit ? $exam : null, 'route' => 'exams'], true) !!}
@endsection

@section('content')
<section class="content">
    <h4>{{ $isEdit ? "EDIT" : "CREATE NEW" }} MODULE</h4>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            @if ($isEdit)
                {!! Form::model($exam, ['route' => ['exams.update', $exam->id], 'method' => 'put']) !!}
                {!! Form::hidden('isEdit', true) !!}
            @else
                {!! Form::open(['route' => ['exams.store'], 'method' => 'post']) !!}
            @endif
            <div class=x_panel>
                <div class=x_title>
                    <strong>Module Properties</strong>
                    <small>Form</small>
                </div>
                <div class=x_content>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group form-float">
                                {!! Form::label('', 'Course Name', ['class' => 'form-label']) !!}
                                {!! Form::select('course_id', $courses, null, ['class' => 'form-control select2']) !!}
                            </div>
                            <div class="form-group form-float">
                                {!! Form::label('', "Description", ['class' => 'form-label']) !!}
                                {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description of exam is here...']) !!}
                            </div>
                            <div class="form-group form-float">
                                {!! Form::label('', "Due Day", ['class' => 'form-label']) !!}
                                {!! Form::date('dday', $isEdit ? $exam->dday : null, ['class' => 'form-control', 'placeholder' => 'Due day is here...']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Submit</button>
                    <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Reset</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</section>
@endsection

@push('js')
{!! Layouts::usePlugin('select2', 'js') !!}
@endpush
