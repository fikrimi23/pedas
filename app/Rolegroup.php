<?php

namespace App;

use App\BaseModel as Model;

class Rolegroup extends Model
{
    /**
     * Fillable column
     *
     * @var array
     */
    protected $fillable = ['rolegroup_name', 'rolegroup_depth'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->route = 'rolegroups';
        $this->moduleName = trans_choice('rolegroups.title', 1);
    }

    public function getRolesStructure($rgId)
    {
        $roles      = Role::where('rolegroup_id', $rgId)->get();
        $roleCooked = [];
        $roleServe  = [];

        foreach ($roles as $role) {
            if (! isset($roleCooked['mod_' . $role->module_id])) {
                $roleCooked['mod_' . $role->module_id] = [
                    'module_id' => $role->module_id,
                    'abilities' => [$role->role_ability]
                ];
            } else {
                array_push($roleCooked['mod_' . $role->module_id]['abilities'], $role->role_ability);
            }
        }

        foreach ($roleCooked as $cooked) {
            array_push($roleServe, $cooked);
        }

        return $roleServe;
    }

    /**
     * Initial relations one to many with users table
     *
     * @return \App\User instance of User;
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }

    /**
     * Initial relations one to many with roles table
     *
     * @return \App\Role instance of Role;
     */
    public function roles()
    {
        return $this->hasMany('App\Role');
    }

    /**
     * Default scope if not super admin
     *
     * @return Instance of query builder
     */
    public function scopeNotSuper()
    {
        return $this->where('rolegroup_depth', ">", 0);
    }
}
