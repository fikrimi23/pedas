<?php

return [
    'profile'   => 'Profile',
    'logout'    => 'Sign Out',

    'title'     => 'Main Navigation',

    'dashboard' => 'Dashboard',

    'users'      => [
        'title'      => 'Users',
        'staffs'     => 'Staffs',
        'users'      => 'Users',
        'rolegroups' => 'Rolegroups',
    ],

    'events'       => 'Events',

    'organizations' => [
        'title'       => 'Organizations',
        'institutes'  => 'Institutes',
        'departments' => 'Departments',
    ],

    'settings'         => [
        'title' => 'Settings',
        'modules' => 'Modules'
    ],
];
