<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\UpdateModuleRequest;
use App\Module;

use Datatables;
use Form;
use DB;
use App\Helper\Flash;

class ModulesController extends Controller
{
    /**
     * @var Name of this module
     */
    private $moduleName;

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        $this->moduleName = trans_choice('modules.title', 1);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('modules.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modules.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\UpdateModuleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdateModuleRequest $request)
    {
        $module = DB::transaction(function () use ($request) {
            $module = Module::create($request->all());
            return $module;
        });
        return redirect()->route('modules.show', $module->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module)
    {
        return view('modules.show')->with('module', $module);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module)
    {
        return view('modules.form')->with('module', $module);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\UpdateModuleRequest  $request
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateModuleRequest $request, Module $module)
    {
        $module = DB::transaction(function () use ($module, $request) {
            $module->update($request->except('module_alias'));
            return $module;
        });
        return redirect()->route('modules.show', $module->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function destroy(Module $module)
    {
        if ($module->locked) {
            Flash::push('error-message', trans('modules.delete_locked'));
            return redirect()->route('modules.index');
        }

        DB::transaction(function () use ($module) {
            $module->delete();
        });
        return redirect()->back();
    }

    /**
     * Get resources for datatables
     *
     * @return JSON Datatables JSON data
     */
    public function getDatatablesResources()
    {
        return Datatables::of(Module::get([
            'id', 'module_alias', 'module_name', 'locked'
            ]))
            ->editColumn('locked', function ($module) {
                if ($module->locked) {
                    return '<span class="badge badge-danger">locked</span>';
                } else {
                    return '<span class="badge badge-success">unlocked</span>';
                }
            })
            ->addColumn('action', function ($module) {
                $settings = [
                    'title' => 'modules',
                    'module' => $module,
                    'part' => [
                        'view' => true,
                        'edit' => auth()->user()->can('update', $module),
                        'delete' => auth()->user()->can('delete', $module),
                    ]
                ];
                $html = view('layouts.datatablesActionColumn')->with($settings)->render();

                return $html;
            })
            ->removeColumn('id')
            ->make(true);
    }
}
