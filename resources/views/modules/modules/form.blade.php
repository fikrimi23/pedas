@extends('layouts.dashboard')

@section('css')
@endsection

@section('breadcrumbs')
{!! Layouts::breadcrumbs(['type' => $isEdit ? 'edit' : 'create', 'module' => $isEdit ? $module : null, 'route' => 'modules'], true) !!}
@endsection

@section('content')
<section class="content">
    <h4>{{ $isEdit ? "EDIT" : "CREATE NEW" }} MODULE</h4>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            @if ($isEdit)
                {!! Form::model($module, ['route' => ['modules.update', $module->id], 'method' => 'put']) !!}
                {!! Form::hidden('isEdit', true) !!}
            @else
                {!! Form::open(['route' => ['modules.store'], 'method' => 'post']) !!}
            @endif
            <div class=x_panel>
                <div class=x_title>
                    <strong>Module Properties</strong>
                    <small>Form</small>
                </div>
                <div class=x_content>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group form-float">
                                {!! Form::label('', 'Module name', ['class' => 'form-label']) !!}
                                {!! Form::text('module_name', null, ['class' => 'form-control', 'placeholder' => 'Module Name']) !!}
                            </div>
                            <div class="form-group form-float">
                                {!! Form::label('', "Module Alias", ['class' => 'form-label']) !!}
                                {!! Form::text('module_alias', null, ['class' => 'form-control', $isEdit ? 'disabled' : '', 'placeholder' => 'modulealias']) !!}
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    {!! Form::hidden('module_core', 0) !!}
                                    {!! Form::checkbox('module_core', true, null, ['class' => 'form-check-input']) !!}
                                    &nbsp;Core
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    {!! Form::hidden('locked', 0) !!}
                                    {!! Form::checkbox('locked', true, null, ['class' => 'form-check-input']) !!}
                                    &nbsp;Locked
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Submit</button>
                    <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Reset</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</section>
@endsection

@push('js')
@endpush
