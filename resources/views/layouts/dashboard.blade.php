{{--
 * CoreUI - Open Source Bootstrap Admin Template
 * @version v1.0.0-alpha.3
 * @link http://coreui.io
 * Copyright (c) 2017 creativeLabs Łukasz Holeczek
 * @license MIT
 *
* {{ config('app.name') }}
 * Author Name  : Fikri Muhammad Iqbal
 * Author email : fikri.miqbal23@gmail.com
 --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    {{-- <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template"> --}}
    {{-- <meta name="author" content="Łukasz Holeczek"> --}}
    {{-- <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard"> --}}
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <title>{{ config('app.name') }}</title>

    {{-- Bootstrap --}}
    <link href="{{ asset('vendor/bootstrap-3.3.7/css/bootstrap.min.css') }}" rel="stylesheet">
    {{-- Tether --}}
    <link href="{{ asset('vendor/tether/css/tether.min.css') }}" rel="stylesheet">
    {{-- Font Awesome --}}
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    {{-- Per page CSS --}}
    @yield('css')

    {{-- Theme --}}
    <link href="{{ asset('vendor/gentelella/css/custom.min.css') }}" rel="stylesheet">

    {{-- Custom --}}
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<!-- BODY options, add following classes to body to change options

// Header options
1. '.header-fixed'                  - Fixed Header

// Sidebar options
1. '.sidebar-fixed'                 - Fixed Sidebar
2. '.sidebar-hidden'                - Hidden Sidebar
3. '.sidebar-off-canvas'        - Off Canvas Sidebar
4. '.sidebar-compact'               - Compact Sidebar Navigation (Only icons)

// Aside options
1. '.aside-menu-fixed'          - Fixed Aside Menu
2. '.aside-menu-hidden'         - Hidden Aside Menu
3. '.aside-menu-off-canvas' - Off Canvas Aside Menu

// Footer options
1. '.footer-fixed'                      - Fixed footer

-->

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title text-uppercase">{{ config('app.name') }}</a>
                    </div>

                    <div class="clearfix"></div>
{{--
                    <div class="profile clearfix">
                        <div class="profile_pic">
                            <img src="{{ asset('images/img.jpg') }}" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2>{{ str_limit(strtok(auth()->user()->staff->name, " ")?? auth()->user()->email, 15) }}</h2>
                        </div>
                    </div> --}}

                    <br>
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>{{ auth()->user()->rolegroup->rolegroup_name }}</h3>
                            <ul class="nav side-menu">
                                {!! $navbar !!}
                            </ul>
                        </div>
                    </div>

                    <div class="sidebar-footer hidden-small">
{{--                         <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a> --}}
                        <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ route('logout') }}">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="top_nav">
                <div class="nav_menu">
                    <nav>
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-left breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Home</a></li>
                                @yield('breadcrumbs')
                            </ol>
                        </ul>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="{{ asset('images/img.jpg') }}" alt="">
                                    {{ auth()->user()->staff->name ?? auth()->user()->email }}
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">
{{--                                     <li><a href="javascript:;"> Profile</a></li>
                                    <li>
                                        <a href="javascript:;">
                                            <span class="badge bg-red pull-right">50%</span>
                                            <span>Settings</span>
                                        </a>
                                    </li>
                                    <li><a href="javascript:;">Help</a></li> --}}
                                    <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>

            {{-- Main content --}}
            <div class="right_col" role="main">
                <div class="">
                    <div class="alert-wrapper m-t-xs">
                    @if (session()->has('success-message'))
                        <div class="alert alert-success" role="alert">
                            <strong>Well done!</strong>
                            <ul>
                            @foreach (session('success-message') as $message)
                                <li>{{ $message }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @elseif (($errors->count() > 0) || (count(session('error-message')) > 0))
                        <div class="alert alert-danger" role="alert">
                            <strong>Oh snap!</strong>
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            @foreach (session('error-message') ?? [] as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    </div>
                    @yield('content')
                    {{-- #container-fluid --}}
                </div>
            </div>
        </div>
    </div>

{{--     <footer class="app-footer">
    <a href="#">{{ config('app.name') }}</a> 2017 CodeBrain.Co.
        <span class="float-right">Powered by <a href="http://coreui.io">CoreUI</a>
        </span>
    </footer> --}}

    {{-- Bootstrap and necessary plugins --}}
    <script src="{{ asset('vendor/jquery/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('vendor/tether/js/tether.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-3.3.7/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendor/bootbox/bootbox.min.js') }}"></script>
    <script src="http://127.0.0.1:35729/livereload.js?snipver=1"></script>

    {{-- Template main scripts --}}
    <script src="{{ asset('js/app.js') }}"></script>

    <script src="{{ asset('js/helpers.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    @include('helper')


    {{-- Per view scripting --}}
    @stack('js')

    {{-- Custom javascript --}}
    <script src="{{ asset('vendor/gentelella/js/custom.js') }}"></script>
    <script type="text/javascript">
        $SIDEBAR_MENU.find('a').filter(function () {
            // alert(this.parent('li').hasClass('current-page'));
            return $(this).parent('li').hasClass('current-page');
        }).parent('li').addClass('current-page').parents('ul').slideDown();

        // recompute content when resizing
        $(window).smartresize(function(){
            setContentHeight();
        });
    </script>
</body>

</html>
