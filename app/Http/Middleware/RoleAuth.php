<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Route;
use Redirect;
use \App\Role;

class RoleAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        $routename  = Route::currentRouteName();

        if ($request->ajax()) {
            list($module, $_trash) = explode('.', $routename);
            $routename = $module.'.index' ;
        }

        $notAuthorizeMessage = trans('auth.unauthorized');

        if (! Role::isAuthorized($user, $routename)) {
            return ($request->ajax()) ? abort(401, $notAuthorizeMessage) : back()->withErrors([$notAuthorizeMessage]);
        }

        return $next($request);
    }
}
