@if ($part['view'] ?? false)
<a
    class="m-l-5 btn-link"
    href="{{ route($title.'.show', $module->id) }}"
    data-toggle="tooltip"
    data-placement="bottom"
    title="View">
    <i class="m-r-sm fa fa-lg fa-eye-open"></i>
    </a>
@endif
@if ($part['edit'] ?? false)
<a
    class="m-l-5 btn-link"
    href="{{ route($title.'.edit', $module->id) }}"
    data-toggle="tooltip"
    data-placement="bottom"
    title="Edit">
    <i class="m-r-sm fa fa-lg fa-pencil"></i>
    </a>
@endif
@if ($part['delete'] ?? false)
{!! Form::open([
    'url' => route($title.'.destroy', $module->id),
    'method'=>'delete',
    'class'=>'form-inline','style="display:inline;"'
    ]) !!}
    <a
        href="#"
        data-confirm="Are you sure to delete this resources?"
        class="m-l-5 btn-link js-delete-confirm"
        data-toggle="tooltip"
        data-placement="bottom"
        title="Delete">
        <i class="m-r-sm fa fa-lg fa-trash"></i>
        </a>
{!! Form::close() !!}
@endif

{!! $part['custom'] ?? '' !!}
