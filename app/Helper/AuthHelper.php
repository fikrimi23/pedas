<?php

namespace App\Helper;

use Auth;

use App\Role;

class AuthHelper
{
    public static function isXUser($ability, $module_alias)
    {
        if (! Auth::check()) {
            return false;
        }

        $user= Auth::user();
        $rgid= $user->rolegroup_id;

        $abl = "X" . strtoupper($ability);

        $role = Role::where('rolegroup_id', $rgid)->where('role_ability', $abl)
        ->whereHas('module', function ($query) use ($module_alias) {
            $query->where('module_alias', $module_alias);
        })->first();

        return isset($role->module->id);
    }
}
