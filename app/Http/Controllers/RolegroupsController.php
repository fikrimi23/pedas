<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

use App\Rolegroup;
use App\Module;
use App\Role;
use App\Helper\Flash;
use App\Http\Requests\UpdateRolegroupRequest;

use Datatables;
use Form;
use DB;

class RolegroupsController extends Controller
{
    /**
     * @var Name of this module
     */
    private $moduleName;

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        $this->moduleName = trans_choice('rolegroups.title', 1);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('rolegroups.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rolegroups.form')->with([
            'modules' => Module::all()
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRolegroupRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdateRolegroupRequest $request)
    {
        $rolegroup = DB::transaction(function () use ($request) {
            $rolegroup = Rolegroup::create($request->all());
            return $rolegroup;
        });

        return redirect()->route('rolegroups.edit', $rolegroup->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rolegroup $rolegroup
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rolegroup $rolegroup
     * @return \Illuminate\Http\Response
     */
    public function edit(Rolegroup $rolegroup)
    {
        $modules = Module::all()->pluck('module_name', 'id');
        $rolegroup->load(['roles', 'roles.module']);
        $abilities = [
            'XREAD' => 'XREAD',
            'XCREATE' => 'XCREATE',
            'XUPDATE' => 'XUPDATE',
            'XDELETE' => 'XDELETE',
            'READ' => 'READ',
            'CREATE' => 'CREATE',
            'UPDATE' => 'UPDATE',
            'DELETE' => 'DELETE',
        ];
        $roles = [];
        foreach ($rolegroup->roles as $role) {
            $roles[$role->module_id][] = $role;
        }
        $roles = collect($roles);

        return view('rolegroups.form')->with([
            'rolegroup' => $rolegroup,
            'modules' => $modules,
            'abilities' => $abilities,
            'rolesCooked' => $roles,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRolegroupRequest  $request
     * @param  \App\Rolegroup $rolegroup
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRolegroupRequest $request, Rolegroup $rolegroup)
    {
        DB::transaction(function () use ($request, $rolegroup) {
            $rolegroup->fill($request->only(['rolegroup_name', 'rolegroup_depth']));
            $rolegroup->save();

            Role::where('rolegroup_id', $rolegroup->id)->delete();
            foreach ($request->abilities as $module_id => $role_abilities) {
                foreach ($role_abilities as $role_ability) {
                    $role = new Role();
                    $role->flashDefault = false;
                    $role->fill([
                        'rolegroup_id' => $rolegroup->id,
                        'module_id' => $module_id,
                        'role_ability' => $role_ability,
                        ]);
                    $role->save();
                }
            }
        });
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rolegroup $rolegroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rolegroup $rolegroup)
    {
        DB::transaction(function () use ($rolegroup) {
            $rolegroup->delete();
        });
        return redirect()->route('rolegroups.index');
    }

    // -- BEGIN -- Custom Function

    /**
     * AJAX datatables resources handler
     *
     * @return JSON Datatable Ready JSON
     */
    public function getDatatablesResources()
    {
        if (\App\Helper\AuthHelper::isXUser('READ', 'rolegroups')) {
            $rolegroups = new Rolegroup();
        } else {
            $rolegroups = Rolegroup::where('rolegroup_depth', '>', '0')->orderBy('rolegroup_depth');
        }

        return Datatables::of($rolegroups->orderBy('rolegroup_depth')->get([
            'id', 'rolegroup_name', 'rolegroup_depth'
            ]))
            ->addColumn('members', function ($rolegroup) {
                return $rolegroup->users()->count();
            })
            ->addColumn('level', function ($rolegroup) {
                return $rolegroup->rolegroup_depth;
            })
            ->addColumn('action', function ($rolegroup) {
                $settings = [
                    'title' => 'rolegroups',
                    'module' => $rolegroup,
                    'part' => [
                        'edit' => auth()->user()->can('update', $rolegroup),
                        'delete' => auth()->user()->can('delete', $rolegroup),
                    ]
                ];
                $html = view('layouts.datatablesActionColumn')->with($settings)->render();
                return $html;
            })
            ->removeColumn('id')
            ->make(true);
    }

    /**
     * Add modules to current rolegroup
     *
     * @param Request $request contain deleted module request
     * @param uuid  $id      Module ID
     */
    public function addModules(Request $request, $id)
    {
        $response = ['status' => false];

        $response = DB::transaction(function () use ($request, $id) {
            $roles = Role::all();
            Role::where('module_id', $request->input('module_id'))->where('rolegroup_id', $id)->delete();
            foreach ($request->input('module_id') as $module_id) {
                $role = new Role([
                    'rolegroup_id' => $id,
                    'module_id' => $module_id,
                    'role_ability' => 'READ'
                    ]);
                $role->flashDefault = false;
                $role->save();
            }

            Flash::push('success-message', 'Module has been added');
            return ['status' => true];
        });

        return collect($response);
    }

    /**
     * Remove modules from current rolegroup
     *
     * @param Request $request contain request
     * @param uuid  $id      Module ID
     */
    public function removeModules(Request $request, $id)
    {
        $response = ['status' => false];

        $response = DB::transaction(function () use ($request, $id) {
            Role::where('rolegroup_id', $id)->where('module_id', $request->input('module_id'))->delete();
            return ['status' => true];
        });
        return collect($response);
    }
}
