<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $group = auth()->user()->group;
        return view('groups.index')->with('group', $group);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->has('action')) {
            if ($request->input('action') == 'join') {
                if (($group = \App\Group::find($request->input('group_id'))) !== null) {
                    \DB::transaction(function () use ($group) {
                        auth()->user()->update(['group_id' => $group->id]);
                    });
                    \Flash::push('success-message', 'You have joined a group');
                } else {
                    \Flash::push('error-message', 'Group didnt exist');
                }
            }
            return back();
        }

        \DB::transaction(function () use ($request) {
            $group = \App\Group::create([
                'name' => $request->input('name'),
                'user_id' => auth()->id()
                ]);

            auth()->user()->update(['group_id' => $group->id]);
        });

        \Flash::push('success-message', 'You have created group');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
