@extends('layouts.dashboard')

@section('css')
{!! Layouts::usePlugin('datatables', 'css') !!}
@endsection

@yield('dependencies')

@section('breadcrumbs')
{!! Layouts::breadcrumbs(['type' => 'index', 'route' => $route], true) !!}
@endsection

@section('content')
<div class="page-title">
    <div class="title_left">
        <h3>@choice($route.'.title', 2)</h3>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>@choice($route.'.title', 2) <small>show all {{ $route }}</small></h2>
                @yield('top_right_button')
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-sm datatable" data-url="{{ route($route.'.datatables') }}">
                    <thead class="thead-inverse">
                        <tr>
                            @foreach ($tableHeader as $header)
                                <th>@lang('datatables.header.'.$header)</th>
                            @endforeach
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

{{-- </section> --}}
@endsection

@push('js')
{!! Layouts::usePlugin('datatables', 'js') !!}
<script>
    $(document).ready(function() {
        var table = $('.datatable');

        table.DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: table.data('url'),
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            columns: datatableSettings,
            language: datatablesLanguage()
          });
    });
</script>
@endpush
