<?php

namespace App;

use App\BaseModel as Model;

class Role extends Model
{
    protected $fillable = ['rolegroup_id', 'module_id','role_ability'];
    protected $with     = ['module'];

    /**
     * Check if specified roles group can use specified resource
     *
     * @param  Auth  $user        user instance to be verified
     * @param  string  $routename string route to check
     * @return boolean            true if authorized
     */
    public static function isAuthorized($user, $routename)
    {
        list($module, $method) = explode(".", $routename);
        $abilities  = [];

        $roles = $user->rolegroup->roles;

        foreach ($roles as $role) {
            if (! isset($abilities[$role->module->module_alias])) {
                $abilities[$role->module->module_alias] = [$role->role_ability];
            } else {
                array_push($abilities[$role->module->module_alias], $role->role_ability);
            }
        }

        if (isset($abilities[$module])) {
            if (($method === 'index') || ($method === 'show')) {
                return true;
            } elseif (($method === 'create') || ($method === 'store')) {
                return (in_array("CREATE", $abilities[$module]) || in_array("XCREATE", $abilities[$module]));
            } elseif (($method === 'edit') || ($method === 'update')) {
                return (in_array("UPDATE", $abilities[$module]) || in_array("XUPDATE", $abilities[$module]));
            } elseif (($method === 'destroy') || ($method === 'shred') || ($method === 'restore')) {
                return (in_array("DELETE", $abilities[$module]) || in_array("XDELETE", $abilities[$module]));
            }
        }

        return false;
    }

    public static function isRoleDepthViolation($model)
    {
        $user       = Auth::user();
        $userRg     = $user->rolegroup;

        if ($model instanceof Rolegroup) {
            $currentRg  = $model;
        } else {
            throw new \Exception("Model is not Rolegroup");
        }

        if ($userRg->rolegroup_depth >= $currentRg->rolegroup_depth) {
            throw new \Exception(trans('rolegroup.depth_violation'));
        }


        return false;
    }

    /**
     * Get users in current ability
     *
     * @param str      $ability
     * @param Module   $module
     * @return array
     */
    public static function getUsersByAbility($ability, Module $module)
    {
        $returned = [];
        $roles = self::where('role_ability', $ability)->where('module_id', $module->id)
            ->with(['rolegroup' => function ($query) {
                $query->with('users');
            }])->get();

        foreach ($roles as $role) {
            $returned[] = $role->rolegroup->users->toArray();
        }

        return $returned;
    }

    /**
     * Many-to-one relation to rolegroup
     *
     * @return \App\Rolegroup
     */
    public function rolegroup()
    {
        return $this->belongsTo('App\Rolegroup');
    }

    /**
     * One-to-one relation to module
     *
     * @return \App\Module
     */
    public function module()
    {
        return $this->belongsTo('App\Module');
    }
}
