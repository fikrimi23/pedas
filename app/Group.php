<?php

namespace App;

use \App\BaseModel as Model;

class Group extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public function host()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function members()
    {
        return $this->hasMany('App\User');
    }
}
