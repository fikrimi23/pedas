<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Staff;
use App\Rolegroup;
use App\Http\Requests\UpdateUserRequest;

use Datatables;
use Form;
use DB;
use Hash;

class UsersController extends Controller
{
    /**
     * @var Name of this module
     */
    private $moduleName;

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        $this->moduleName = trans_choice('users.title', 1);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $staffs = Staff::where(DB::raw('user_id IS NOT NULL'))->get();
        $rolegroups = Rolegroup::notSuper()->get();
        return view('users.form')->with([
            'staffs' => ['' => 'Create New Profile'], $staffs->pluck('name', 'id')->toArray(),
            'rolegroups' => $rolegroups->pluck('rolegroup_name', 'id'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdateUserRequest $request)
    {
        $user = DB::transaction(function () use ($request) {
            // create new User
            $user = new User();
            $user->fill($request->only(['email', 'password']));
            $user->rolegroup_id = $request->input('rolegroup.id');
            $user->password = Hash::make($user->password);
            $user->save();

            // create new staff and associate to already created user
            $staff = new Staff($request->input('staff'));
            $staff->user()->associate($user);
            $staff->save();

            return $user;
        });
        return redirect()->route('users.edit', $user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        // return view('users.show')->with('module', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user->load(['rolegroup', 'staff']);

        $staffs = Staff::where(DB::raw('user_id IS NOT NULL'))->get();
        $rolegroups = Rolegroup::notSuper()->get();
        return view('users.form')->with([
            'user' => $user,
            'staffs' => ['' => 'Create New Profile'], $staffs->pluck('name', 'id')->toArray(),
            'rolegroups' => $rolegroups->pluck('rolegroup_name', 'id'),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $user = DB::transaction(function () use ($request, $user) {
            // autofill user
            $user->fill($request->only('email', 'password'));

            // check for password
            if (empty($user->password)) {
                unset($user->password);
            } else {
                $user->password = Hash::make($user->password);
            }

            // fill relation id
            if ($user->rolegroup->rolegroup_depth > 0) {
                $user->rolegroup_id = $request->input('rolegroup.id');
            }
            $user->save();

            // update child
            $user->staff->update($request->input('staff'));
            return $user;
        });
        return redirect()->route('users.edit', $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        DB::transaction(function () use ($user) {
            $user->delete();
        });
        return redirect()->route('users.index');
    }

    /* BEGIN custom function */
    /**
     * Get resources for datatables
     *
     * @return JSON Datatables JSON data
     */
    public function getDatatablesResources()
    {
        return Datatables::of(User::get([
            'id', 'email', 'is_active'
            ]))
            ->editColumn('is_active', function ($user) {
                if ($user->is_active) {
                    return '<span class="badge badge-success">Active</span>';
                } else {
                    return '<span class="badge badge-danger">Not Active</span>';
                }
            })
            ->addColumn('action', function ($user) {
                $settings = [
                    'title' => 'users',
                    'module' => $user,
                    'part' => [
                        'edit' => true,
                        'delete' => true,
                    ]
                ];
                $html = view('layouts.datatablesActionColumn')->with($settings)->render();
                return $html;
            })
            ->removeColumn('id')
            ->make(true);
    }
}
