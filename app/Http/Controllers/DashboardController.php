<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $time = \Carbon\Carbon::now('Asia/Jakarta');

        switch ($time->format('H')) {
            case $time < "12":
                $welcome = "Good Morning";
                break;
            case $time >= "12" && $time < "17":
                $welcome = "Good Morning";
                break;
            case $time >= "17" && $time < "19":
                $welcome = "Good Afternoon";
                break;
            case $time >= "19":
                $welcome = "Good Evening";
                break;
            default:
                $welcome = "Hello";
                break;
        }

        $courses = \App\Course::where('user_id', auth()->id())->withCount(['tasks' => function ($task) {
            return $task->where('status', '0');
        }, 'exams'])->with(['tasks', 'exams'])->get();
        $tasks_count = $exams_count = 0;

        $now = \Carbon\Carbon::now('Asia/Jakarta');

        $tasks = $courses->load(['tasks' => function ($task) {
            $now = \Carbon\Carbon::now('Asia/Jakarta');
            // ddr($now->addDay(7));
            return $task->where('status', 0)->where('deadline', '>=', $now->format('Y/m/d'))->where('deadline', '<=', $now->addDay(7)->format('Y/m/d'));
        }, 'tasks.course'])->pluck('tasks')->collapse();

        $exams = $courses->load(['exams' => function ($exam) {
            $now = \Carbon\Carbon::now('Asia/Jakarta');
            return $exam->where('dday', '>=', $now->format('Y/m/d'))->where('dday', '<=', $now->addDay(7)->format('Y/m/d'));
        }])->pluck('exams')->collapse();

        $courses->each(function ($course) use (&$tasks_count, &$exams_count) {
            $tasks_count += $course->tasks_count;
            $exams_count += $course->exams_count;
        });

        return view('dashboard.index')->with([
            'courses' => $courses,
            'tasks' => $tasks,
            'exams' => $exams,
            'today_courses' => $courses->where('day', \Carbon\Carbon::now('Asia/Jakarta')->format('N')),
            'welcome' => $welcome,
            'tasks_count' => $tasks_count,
            'exams_count' => $exams_count,
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
