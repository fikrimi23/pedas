<?php

use Illuminate\Database\Seeder;

class StaffsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $staff = DB::table('staffs');
        $staff->delete();

        $records = [
            [
                'user_id'    => App\User::where('email', 'super.admin@email.com')->first()->id,
                'name'       => 'Fikri Muhammad Iqbal',
            ],
            [
                'user_id'    => App\User::where('email', 'general.admin@email.com')->first()->id,
                'name'       => 'General Admin',
            ]
        ];

        collect($records)->each(function ($record) {
            App\Staff::create($record);
        });
    }
}
