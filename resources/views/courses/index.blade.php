@extends('layouts.index.datatable')

@section('dependencies')
    @php
        $route = 'courses';
        $tableHeader = [
            'course',
            'lecturer',
            'room',
            'day',
            'time',
            'action',
        ];
    @endphp
@endsection

@section('top_right_button')
@can('create', new \App\Course())
    <a href="{{ route($route.'.create') }}" class="btn btn-primary btn-md pull-right">
        <i class="fa fa-fw fa-user-plus"></i> New @choice($route.'.title', 1)
    </a>
@endcan
@endsection



@push('js')
<script>
datatableSettings = [
    {data: 'title', name: 'course'},
    {data: 'lecturer', name: 'lecturer'},
    {data: 'room', name: 'room'},
    {data: 'day', name: 'day'},
    {data: 'time', name: 'time'},
    {data: 'action', name: 'Action', sortable: false, searchable: false},
]
</script>
@endpush
