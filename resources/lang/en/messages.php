<?php

return [
    'success' => [
        'create' => 'New :module has been created',
        'update' => ':module has been updated',
        'delete' => ':module has been deleted',
    ],
    'fail' => [
        'create' => ':module creation failed, please try again later or contact Admin',
        'update' => ':module update failed, please try again later or contact Admin',
        'delete' => ':module delete failed, please try again later or contact Admin',
    ]
];
