<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Model;
use App\Traits\Uuids;
use App\Traits\FlashMessages;

class BaseModel extends Model
{
    use Uuids, FlashMessages;

    /**
     * Set incrementing to false because of UUID
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Set flashdefault to true, make inherited model always use Flash Messages
     *
     * @var boolean
     */
    public $flashDefault = true;

    /**
     * Route name associated in use for policy
     *
     * @var string
     */
    public $route = '';

    /**
     * moduleName associated in use for messages
     *
     * @var string
     */
    public $moduleName = '';

    /**
     * Hide sensitive attribute
     *
     * @var array
     */
    public $hidden = [
        'id', 'created_at', 'updated_at'
    ];

    /**
     * Default column parsed as date
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
