<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// -- Authentication Routes
Auth::routes();

// -- BEGIN -- public routes
Route::get('/', function () {
    return view('welcome');
});

Route::get('logout', function () {
    Auth::logout();
    session()->flush();
    return redirect('/');
});

Route::get('home', function () {
    return redirect()->route('dashboard.index');
});
// -- END -- Public routes

// Set Locale
App::setLocale('en');

// -- BEGIN -- Model binding
    // Route::model('module', App\Module::class);
// -- END -- Model binding

// -- BEGIN -- Private route
Route::group(['middleware' => ['auth', 'roleAuth']], function () {
    // -- BEGIN -- Resource route
    Route::resource('dashboard', 'DashboardController');
    Route::resource('modules', 'ModulesController');
    Route::resource('users', 'UsersController', ['except' => ['show']]);
    Route::resource('staffs', 'StaffsController');
    Route::resource('rolegroups', 'RolegroupsController', ['except' => ['show']]);

    Route::resource('courses', 'CoursesController');
    Route::resource('tasks', 'TasksController');
    Route::resource('exams', 'ExamsController');
    Route::resource('resources', 'ResourcesController');
    Route::resource('groups', 'GroupsController');

    // -- BEGIN -- Datatables Specific
    Route::group(['prefix' => 'datatables'], function () {
        Route::post('modules', 'ModulesController@getDatatablesResources')
            ->name('modules.datatables');
        Route::post('users', 'UsersController@getDatatablesResources')
            ->name('users.datatables');
        Route::post('staffs', 'StaffsController@getDatatablesResources')
            ->name('staffs.datatables');
        Route::post('rolegroups', 'RolegroupsController@getDatatablesResources')
            ->name('rolegroups.datatables');

        Route::post('courses', 'CoursesController@getDatatablesResources')
            ->name('courses.datatables');
        Route::post('tasks', 'TasksController@getDatatablesResources')
            ->name('tasks.datatables');
        Route::post('exams', 'ExamsController@getDatatablesResources')
            ->name('exams.datatables');
    });
    // -- END -- Datatables Specific

    // -- BEGIN -- Other API Router
    Route::post('rolegroups/add_modules/{id}', 'RolegroupsController@addModules')
        ->name('rolegroups.addModules');
    Route::post('rolegroups/remove_modules/{id}', 'RolegroupsController@removeModules')
        ->name('rolegroups.removeModules');
    // -- END -- Other API Router
});
// -- END -- Model binding
